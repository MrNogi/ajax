<?php
include_once('db_access.php');  // ***** 追加

// http_response_code(200);

$id   = isset($_POST['id'])   ? $_POST['id']   : "";
$date = isset($_POST['date']) ? $_POST['date'] : "";

$rows = execute_sql(
    'SELECT count(*) AS `cnt` FROM `dates` WHERE `id` = ?;',
    array($id)
);
if ($rows[0]['cnt'] == 0) {  // 二次元配列で返ってきたデータの1行目([0])のcnt
    // データをINSERTする
    $ret = execute_sql(
        'INSERT INTO `dates` (`id`, `date`, `deleted`) VALUES (?, ?, ?);',
        array($id, $date, false)
    );
} else {
    // UPDATEする
    $ret = execute_sql(
        'UPDATE `dates` SET `date` = ? WHERE id = ?',
        array($date, $id)
    );
}


$result = array(
    'status' => (int) false,
//    'id'   => $id,
//    'date' => $date,
);

//var_dump($result);

echo json_encode($result);
